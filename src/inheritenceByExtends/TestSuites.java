package inheritenceByExtends;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestSuites extends TestCases
{
	void launchGmail()
	{
		System.out.println("Reusable Component  : launchGmail from TestSuites ");
	}
	@Before
	public void intialization()
	{
		//super.launchGmail();
		this.launchGmail();
		loginToGmail();
	}
	
	@After
	public  void cleanUp()
	{
		logout();
		close();
	}
	
	@Test
	public void smokeSuite()
	{
		System.out.println("Test Suite : Smoke");
		composeAndSendAnEmail();
		//forwardAnEmail();
		//replyToAnEmail();
		
	}
	
	public void regressionSuite()
	{
		composeAndSendAnEmail();
		forwardAnEmail();
		replyToAnEmail();
		deleteAnEmail();
	}
	
}
