package inheritenceByExtends;

public class ReusableComponents
{
	//******************Reusable Components ************
			  void launchGmail()
			{
				System.out.println("Reusable Component : launchGmail from RC");
			}
			 void loginToGmail()
			{
				System.out.println("Reusable Component : loginToGmail ");
			}
			protected  void compose()
			{
				System.out.println("Reusable Component : compose ");
			}
			public  void send()
			{
				System.out.println("Reusable Component : send ");
			}
			public  void open()
			{
				System.out.println("Reusable Component : open ");
			}
			public  void forward()
			{
				System.out.println("Reusable Component : forward ");
			}
			public  void delete()
			{
				System.out.println("Reusable Component : delete ");
			}
			public  void logout()
			{
				System.out.println("Reusable Component : logout ");
			}
			public  void close()
			{
				System.out.println("Reusable Component : close ");
			}
			public  void reply()
			{
				System.out.println("Reusable Component : reply ");
			}
			

}
