package inheritenceByExtends;

import org.junit.Test;

public class TestCases extends ReusableComponents
{
	void launchGmail()
	{
		System.out.println("Reusable Component  : launchGmail from TestCases ");
	}
	//***********Test Cases ***************
			@Test
			public  void composeAndSendAnEmail()
			{
				System.out.println("Test Case : composeAndSendAnEmail ");
				//launchGmail();
				//loginToGmail();
				compose();
				send();
				//logout();
				//close();
			}
			@Test
			public  void forwardAnEmail()
			{
				System.out.println("Test Case : forwardAnEmail ");
				//launchGmail();
				//loginToGmail();
				open();
				forward();
				//logout();
				//close();
			}
			@Test
			public  void replyToAnEmail()
			{
				System.out.println("Test Case : replyToAnEmail ");
				//launchGmail();
				//loginToGmail();
				open();
				reply();
				//logout();
				//close();
				
			}
			@Test
			public  void deleteAnEmail()
			{
				System.out.println("Test Case : deleteAnEmail ");
				//launchGmail();
				//loginToGmail();
				open();
				delete();
				//logout();
				//close();
			}
			

}
