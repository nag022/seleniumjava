package coreJavaPractice;

import org.junit.Test;

public class LoopingStatements
{
	@Test
	public void printFirstFiveNaturalNumbers()
	{
		System.out.println("First Natural Number is : 1");
		System.out.println("Second Natural Number is : 2");
		System.out.println("Three Natural Number is : 3");
		System.out.println("Four Natural Number is : 4");
		System.out.println("Fifth Natural Number is : 5");
	}
	@Test
	public void printFirstTenNaturalNumbers()
	{
		System.out.println("First Natural Number is : 1");
		System.out.println("Second Natural Number is : 2");
		System.out.println("Three Natural Number is : 3");
		System.out.println("Four Natural Number is : 4");
		System.out.println("Fifth Natural Number is : 5");
		
		System.out.println("First Natural Number is : 1");
		System.out.println("Second Natural Number is : 2");
		System.out.println("Three Natural Number is : 3");
		System.out.println("Four Natural Number is : 4");
	}
	
	@Test
	public void printFirstFiveNaturalNumbersLoop()
	{
		for(int i=1;i<=5;i = i + 1)
		{
			System.out.println("My" + i + " th natural number is :" + i);
		}
	}
	@Test
	public void workWithForLoop()
	{
		//printNaturalNumbers(25);
		printEvenNumbers(10);
	}
	
	
	public void printNaturalNumbers(int howMany)
	{
		for(int i=1;i<=howMany*2;i = i + 1)
		{
			System.out.println("My " + i + " th natural number is :" + i);
		}
	}
	public void printEvenNumbers(int howMany)
	{
		for(int i=0;i<=howMany;i = i + 2)
		{
			System.out.println("My " + i + " th even number is :" + i);
		}
	}
	@Test
	public void workWithDoLoop()
	{
		//printEvenNumbersDoLoop(20);
		printOddNumbersDoLoop(10);
	}
	public void printEvenNumbersDoLoop(int howMany)
	{
		//for(int i=0;i<=howMany;i = i + 2)
		int counter = 1;
		int i =0;
		do
		{
			System.out.println("My " + i + " th even number is :" + i);
			i = i+2;
			counter = counter + 1;
			
		}
		while(counter<howMany);
		System.out.println("Count :" + counter);
	}
	public void printOddNumbersDoLoop(int howMany)
	{
		//for(int i=0;i<=howMany;i = i + 2)
		int counter = 1;
		int i =1;
		do
		{
			System.out.println("My " + i + " th even number is :" + i);
			i = i+2;
			counter = counter + 1;
			
		}
		while(counter<howMany);
		System.out.println("Count :" + counter);
	}
	@Test
	public void loopWithCondition()
	{
		//flag = findStudent(9);
		if(findStudent(9))
		{
			System.out.println("Student found ");
		}
		else
		{
			System.out.println("Did not Student found :");
		}
	}
	boolean flag;
	public boolean findStudent(int stdNum)
	{
		for(int i =1; i<20;i++)
		{
			//System.out.println("Students searched :" + i);
			if(i== stdNum)
			{
				//System.out.println("Student found :"+ i);
				flag = true;
				break;
			}
			
		}
		
		return flag;
	}
}
