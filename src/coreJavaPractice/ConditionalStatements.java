package coreJavaPractice;

import org.junit.Test;

public class ConditionalStatements 
{
	int a = 25;
	int b = 22;
	
	@Test
	public void ifCondition()
	{
		if(a == b)
		{
			System.out.println("Both A and B are equal");
			System.out.println("Sum of A and B :" + (a + b));
		}
			
	}
	
	@Test
	public void workWithIfElse()
	{
		ifElseCondition(22,28);
		ifElseCondition(22,22);
	}
	public void ifElseCondition(int a ,int b)
	{
		if(a == b)
		{
			System.out.println("Both A and B are equal");
			System.out.println("Sum of A and B :" + (a + b));
		}
		else
		{
			System.out.println("Both A and B are not equal");
			System.out.println("Sum of A and B :" + (a - b));
		}
			
	}
	@Test
	public void workWithElseIf()
	{
		elseIfCondition(22,28);
		elseIfCondition(22,22);
		elseIfCondition(28,22);
	}
	public void elseIfCondition(int a ,int b)
	{
		if(a == b)
		{
			System.out.println("Both A and B are equal");
			System.out.println("Sum of A and B :" + (a + b));
		}
		else if(a>b)
		{
			System.out.println("A is above B");
			System.out.println("A - B :" + (a - b));
		}
		else if(a<b)
		{
			System.out.println("A is below B");
			System.out.println("B - A :" + (b - a));
		}
		else
		{
			System.out.println("Sum of A and B :" + (a + b));
		}
			
	}
	@Test
	public void workWithSwitchCase()
	{
		switchCase('A');
		switchCase('B');
		switchCase('C');
		switchCase('D');
		switchCase('E');
		switchCase('F');
		switchCase('G');
		switchCase('K');
		
	}
	
	public void switchCase(char grade)
	{
		switch(grade)
		{
		case 'A' : 
		{
			System.out.println("Student Passed in First Class With Distinction, Grade :" + grade);
			break;
		}
		case 'B' : 
		{
			System.out.println("Student Passed in First Class , Grade :" + grade);
			break;
		}
		case 'C' : 
		{
			System.out.println("Student Passed in Second Class , Grade :" + grade);
			break;
		}
		case 'D' : 
		{
			System.out.println("Student Passed in Third Class , Grade :" + grade);
			break;
		}
		case 'E' : 
		{
			System.out.println("Student Passed in ordinary, Grade :" + grade);
			break;
		}
		case 'F' : 
		{
			System.out.println("Student Failed, Grade :" + grade);
			break;
		}
		case 'G' : 
		{
			System.out.println("Result is unknown, Grade :" + grade);
			break;
		}
		default : 
		{
			System.out.println("Result is unknown, Grade :" + grade);
			break;
		}
		}
		
	}

}
