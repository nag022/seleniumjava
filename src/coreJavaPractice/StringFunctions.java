package coreJavaPractice;

import org.junit.Test;

public class StringFunctions
{
	String a = "  Selenium With Ruby   ";
	String b = "Selenium With JAVA";
	String c = "Java ";
	@Test
	public void StringSplit()
	{
		String[] subStrings = a.trim().split(" ");
		for(int i=0;i<subStrings.length;i++)
		{
			System.out.println(subStrings[i]);
		}
		
		System.out.println("Todays Technology is :" + subStrings[2]);
	}
	@Test
	public void stringContains()
	{
		if(a.toLowerCase().trim().contains(c.toLowerCase().trim()))
		{
			System.out.println("Todays technology is Java");
		}
		else
		{
			System.out.println("Todays technology is not Java");
		}
	}
	
	@Test
	public void stringComparision()
	{
		//if(a.trim().toLowerCase().equals(b.toLowerCase().trim()))
		if(a.trim().equalsIgnoreCase(b.trim()))
		{
			System.out.println("Both the strings are  equal");
		}
		else
		{
			System.out.println("Both the strings are not equal");
		}
	}
	@Test
	public void stringlength()
	{
		int len = a.length();
		System.out.println("My String length before trim :" + len);
		a = a.trim();
		System.out.println("My String length after trim :" + a.length());
		String aL = a.toLowerCase();
		System.out.println("My String after lower conversion :" + aL);
		String aU = a.toUpperCase();
		System.out.println("My String after upper conversion :" + aU);
	}

}
