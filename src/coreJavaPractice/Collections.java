package coreJavaPractice;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

public class Collections 
{
	@Before
	public void intialization()
	{
		//System.out.println("Current test is " + getName());
	}
	@Test
	public void staticIntArray()
	{
		//System.out.println("Current test is " + getName());
		int[] numbers = {1,2,3,4,5,6};
		int len = numbers.length;
		System.out.println("My String length :" + len);
		for(int i=0;i<len;i++)
		{
			int value = numbers[i];
			System.out.println("My :" + i + " : th Element before writing :" + value);
		}
		
		for(int i=0;i<len;i++)
		{
			 numbers[i] = 4+i;
			System.out.println("My :" + i + " : th Element after writing :" + numbers[i]);
		}
	}
	@Test
	public void staticStringArray()
	{
		String[] names = {"Ram","Ravi","Raju","Raj"};
		int len = names.length;
		System.out.println("My String length :" + len);
		for(int i=0;i<len;i++)
		{
			String value = names[i];
			System.out.println("My :" + i + " : th Element before writing :" + value);
		}
		
		for(int i=0;i<len;i++)
		{
			names[i] = "Edge "+i;
			System.out.println("My :" + i + " : th Element after writing :" + names[i]);
		}
	}
	
	@Test
	public void dynamicIntArray()
	{
		//int[] numbers = {1,2,3,4,5,6};
		int[] numbers = new int[10];
		int len = numbers.length;
		System.out.println("My String length :" + len);
		for(int i=0;i<len;i++)
		{
			int value = numbers[i];
			System.out.println("My :" + i + " : th Element before writing :" + value);
		}
		
		for(int i=0;i<len;i++)
		{
			 numbers[i] = 4+i;
			System.out.println("My :" + i + " : th Element after writing :" + numbers[i]);
		}
	}
	@Test
	public void dynamicStringArray()
	{
		//String[] names = {"Ram","Ravi","Raju","Raj"};
		String[] names = new String[10];
		int len = names.length;
		System.out.println("My String length :" + len);
		for(int i=0;i<len;i++)
		{
			String value = names[i];
			System.out.println("My :" + i + " : th Element before writing :" + value);
		}
		
		for(int i=0;i<len;i++)
		{
			names[i] = "Edge "+i;
			System.out.println("My :" + i + " : th Element after writing :" + names[i]);
		}
	}
	
	@Test
	public void intArrayList()
	{
		//int[] numbers = {1,2,3,4,5,6};
		//int[] numbers = new int[10];
		List<Integer>  numbers = new ArrayList<Integer>();
		int len = numbers.size();
		System.out.println("My String length :" + len);
		/*for(int i=0;i<10;i++)
		{
			int value = numbers.get(i);
			System.out.println("My :" + i + " : th Element before writing :" + value);
		}*/
		
		for(int i=0;i<10;i++)
		{
			 numbers.add(4 + i);
			System.out.println("My :" + i + " : th Element after writing :" + numbers.get(i));
		}
		System.out.println("My String length after :" + numbers.size());
	}
	
	@Test
	public void stringArrayList()
	{
		//int[] numbers = {1,2,3,4,5,6};
		//int[] numbers = new int[10];
		List<String>  names = new ArrayList<String>();
		int len = names.size();
		System.out.println("My String length :" + len);
		/*for(int i=0;i<10;i++)
		{
			int value = numbers.get(i);
			System.out.println("My :" + i + " : th Element before writing :" + value);
		}*/
		
		for(int i=0;i<10;i++)
		{
			names.add("Edge :" + i);
			System.out.println("My :" + i + " : th Element after writing :" + names.get(i));
		}
		System.out.println("My String length after :" + names.size());
	}
}
