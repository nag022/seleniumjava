package coreJavaPractice;

public class GmailWithMainMethod 
{
	
	public static void main(String[] args)
	{
		composeAndSendAnEmail();
		forwardAnEmail();
		replyToAnEmail();
		deleteAnEmail();
		
	}
	//***********Test Cases ***************
	public static void composeAndSendAnEmail()
	{
		System.out.println("Test Case : composeAndSendAnEmail ");
		launchGmail();
		loginToGmail();
		compose();
		send();
		logout();
		close();
	}
	public static void forwardAnEmail()
	{
		System.out.println("Test Case : forwardAnEmail ");
		launchGmail();
		loginToGmail();
		open();
		forward();
		logout();
		close();
	}
	public static void replyToAnEmail()
	{
		System.out.println("Test Case : replyToAnEmail ");
		launchGmail();
		loginToGmail();
		open();
		reply();
		logout();
		close();
		
	}
	public static void deleteAnEmail()
	{
		System.out.println("Test Case : deleteAnEmail ");
		launchGmail();
		loginToGmail();
		open();
		delete();
		logout();
		close();
	}
	
	//******************Reusable Components ************
	public static void launchGmail()
	{
		System.out.println("Reusable Component : launchGmail ");
	}
	public static void loginToGmail()
	{
		System.out.println("Reusable Component : loginToGmail ");
	}
	public static void compose()
	{
		System.out.println("Reusable Component : compose ");
	}
	public static void send()
	{
		System.out.println("Reusable Component : send ");
	}
	public static void open()
	{
		System.out.println("Reusable Component : open ");
	}
	public static void forward()
	{
		System.out.println("Reusable Component : forward ");
	}
	public static void delete()
	{
		System.out.println("Reusable Component : delete ");
	}
	public static void logout()
	{
		System.out.println("Reusable Component : logout ");
	}
	public static void close()
	{
		System.out.println("Reusable Component : close ");
	}
	public static void reply()
	{
		System.out.println("Reusable Component : reply ");
	}
	

}
