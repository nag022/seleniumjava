package coreJavaPractice;

public class CoreJavaPractice 
{
	public int empNumber = 22;
	protected String empName = "Ram";
	private char grade = 'A';
	static int result ;
	static int sum;
	
	public static void  main(String[] arguments)
	{
		addition();
		addition(46);
		addition(85,38);
		addition(53,767,435);
		result = addition1();
		System.out.println("My result :" + result);
		result = addition1(4554,4543);
		System.out.println("My result :" + result);
		
	}
	
	//00,01,10,11
	public static  void addition()
	{
		 sum = 22 + 26;
		System.out.println("Sum of two static numbers :" + sum);
	}
	public static  void addition(int a)
	{
		 sum = 22 + a;
		System.out.println("Sum of two numbers with 22 and " + a + " :" + sum);
	}
	public static  void addition(int a,int b)
	{
		 sum = a + b;
		System.out.println("Sum of two numbers " + a + "," + b  +" : "+ sum);
	}
	public static  void addition(int a,int b,int c)
	{
		 sum = a + b + c;
		System.out.println("Sum of three numbers " + a + "," + b  + "," + c + " : "+ sum);
	}
	
	public static  int  addition1()
	{
		 sum = 22 + 26;
		//System.out.println("Sum of two static numbers :" + sum);
		return sum;
	}
	public static  int  addition1(int a,int b)
	{
		 sum = a + b;
		//System.out.println("Sum of two static numbers :" + sum);
		return sum;
	}

}

