package coreJavaPractice;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GmailWithJunit
{
	/*public static void main(String[] args)
	{
		login();
	}
	*/
	@Before
	public void intialization()
	{
		launchGmail();
		loginToGmail();
	}
	
	@After
	public  void cleanUp()
	{
		logout();
		close();
	}
	
	//***********Test Cases ***************
		@Test
		public  void composeAndSendAnEmail()
		{
			System.out.println("Test Case : composeAndSendAnEmail ");
			//launchGmail();
			//loginToGmail();
			compose();
			send();
			//logout();
			//close();
		}
		@Test
		public  void forwardAnEmail()
		{
			System.out.println("Test Case : forwardAnEmail ");
			//launchGmail();
			//loginToGmail();
			open();
			forward();
			//logout();
			//close();
		}
		@Test
		public  void replyToAnEmail()
		{
			System.out.println("Test Case : replyToAnEmail ");
			//launchGmail();
			//loginToGmail();
			open();
			reply();
			//logout();
			//close();
			
		}
		@Test
		public  void deleteAnEmail()
		{
			System.out.println("Test Case : deleteAnEmail ");
			//launchGmail();
			//loginToGmail();
			open();
			delete();
			//logout();
			//close();
		}
		
		//******************Reusable Components ************
		public  void launchGmail()
		{
			System.out.println("Reusable Component : launchGmail ");
		}
		public  void loginToGmail()
		{
			System.out.println("Reusable Component : loginToGmail ");
		}
		public  void compose()
		{
			System.out.println("Reusable Component : compose ");
		}
		public  void send()
		{
			System.out.println("Reusable Component : send ");
		}
		public  void open()
		{
			System.out.println("Reusable Component : open ");
		}
		public  void forward()
		{
			System.out.println("Reusable Component : forward ");
		}
		public  void delete()
		{
			System.out.println("Reusable Component : delete ");
		}
		public  void logout()
		{
			System.out.println("Reusable Component : logout ");
		}
		public  void close()
		{
			System.out.println("Reusable Component : close ");
		}
		public  void reply()
		{
			System.out.println("Reusable Component : reply ");
		}
		

	
}
