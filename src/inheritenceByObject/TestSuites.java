package inheritenceByObject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestSuites 
{
	TestCases myTestCases = new TestCases();
	ReusableComponents myRc = new ReusableComponents();
	void launchGmail()
	{
		System.out.println("Reusable Component  : launchGmail from TestSuites ");
	}
	@Before
	public void intialization()
	{
		
		launchGmail();
		myRc.loginToGmail();
	}
	
	@After
	public  void cleanUp()
	{
		myRc.logout();
		myRc.close();
	}
	
	@Test
	public void smokeSuite()
	{
		System.out.println("Test Suite : Smoke");
		myTestCases.composeAndSendAnEmail();
		myTestCases.forwardAnEmail();
		myTestCases.replyToAnEmail();
		
	}
	
	public void regressionSuite()
	{
		myTestCases.composeAndSendAnEmail();
		myTestCases.forwardAnEmail();
		myTestCases.replyToAnEmail();
		myTestCases.deleteAnEmail();
	}
	
}
