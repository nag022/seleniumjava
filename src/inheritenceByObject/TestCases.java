package inheritenceByObject;

import org.junit.Test;

public class TestCases 
{
	public TestCases()
	{
		System.out.println("Deafault Constructor : Test Cases");
	}
	public TestCases(String browser)
	{
		System.out.println("Overloaded Constructor : Test Cases, Browser :"+ browser);
	}
	ReusableComponents myRc = new ReusableComponents();
	void launchGmail()
	{
		System.out.println("Reusable Component  : launchGmail from TestCases ");
	}
	//***********Test Cases ***************
			@Test
			public  void composeAndSendAnEmail()
			{
				System.out.println("Test Case : composeAndSendAnEmail ");
				//launchGmail();
				//loginToGmail();
				
				myRc.compose();
				myRc.send();
				//logout();
				//close();
			}
			@Test
			public  void forwardAnEmail()
			{
				System.out.println("Test Case : forwardAnEmail ");
				//launchGmail();
				//loginToGmail();
				myRc.open();
				myRc.forward();
				//logout();
				//close();
			}
			@Test
			public  void replyToAnEmail()
			{
				System.out.println("Test Case : replyToAnEmail ");
				//launchGmail();
				//loginToGmail();
				myRc.open();
				myRc.reply();
				//logout();
				//close();
				
			}
			@Test
			public  void deleteAnEmail()
			{
				System.out.println("Test Case : deleteAnEmail ");
				//launchGmail();
				//loginToGmail();
				myRc.open();
				myRc.delete();
				//logout();
				//close();
			}
			

}
