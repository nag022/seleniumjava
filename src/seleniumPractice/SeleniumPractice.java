package seleniumPractice;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.junit.Before;
//import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class SeleniumPractice
{
	WebDriver driver;
	ReadProperties props = new ReadProperties("TestData.properties");
	ApplicationUtilities myUtilis;
	public SeleniumPractice()
	{
		System.setProperty("webdriver.chrome.driver", "D:\\WorkSpace\\Java\\SeleniumJarFiles\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		//System.setProperty("webdriver.gecko.driver", "D:\\WorkSpace\\Java\\SeleniumJarFiles\\geckodriver.exe");
		//System.setProperty("webdriver.firefox.marionette", "D:\\WorkSpace\\Java\\SeleniumJarFiles\\geckodriver.exe");
		//driver = new FirefoxDriver();
		myUtilis = new ApplicationUtilities(driver);
	}
	public void newGitRepo()
	{
		System.out.println("New Git Repo added");
	}
	@Test
	public void mouseEvents()
	{
		Actions myActions = new Actions(driver);
		WebElement userName = driver.findElement(By.name("userName"));
		myActions.moveToElement(userName).click().sendKeys("nag022").build().perform();
		myActions.moveToElement(userName).click().contextClick().build().perform();
		
	}
	//added  a comment
	@Test
	public void gmailFileUpload() throws IOException
	{
		//Runtime.getRuntime().exec("E:\\AutoIT\\FileUpload.exe");
		Runtime.getRuntime().exec("D:\\WorkSpace\\Java\\Projects\\AutoIT\\ChromeOpenFile.exe");


	}
	@Test
	public void dataBaseConnection() throws ClassNotFoundException, SQLException
	{
		Class.forName("com.mysql.jdbc.Driver");  
		//Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/world","root","12345");  
		Connection con=DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/world","root","12345");
		//here sonoo is database name, root is username and password  
		Statement stmt=con.createStatement();  
		ResultSet rs=stmt.executeQuery("select * from city");  
		while(rs.next())  
		System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3));  
		con.close();  
		 
	}
	@Test
	public void readXlData() throws BiffException, IOException
	{
		FileInputStream myFile = new FileInputStream("TestData/TestData.xls");
		Workbook myBook = Workbook.getWorkbook(myFile);
		Sheet mySheet = myBook.getSheet("DEV");
		int colCount = mySheet.getColumns();
		System.out.println("Column count :" + colCount);
		int rc = mySheet.getRows();
		System.out.println("Row count :" + rc);
		System.out.println(mySheet.getCell(0, 1).getContents());
		System.out.println(mySheet.getCell(1, 1).getContents());
		System.out.println(mySheet.getCell(2, 1).getContents());
	}
	@BeforeMethod
	public void lauchApplication()
	{
		//driver.get("http://newtours.demoaut.com/");
		driver.get(props.readProperties("URL"));
	}
	@Test
	public void loginApplication_WithReturnWebElement()
	{
		myUtilis.returnWebElement("name","userName").sendKeys(props.readProperties("UserName"));
		myUtilis.returnWebElement("name","password").sendKeys(props.readProperties("PassWord"));
		//myUtilis.returnWebElement("name","login").click();
		myUtilis.returnWebElement("name","login").sendKeys(Keys.ENTER);
	}
	//@Test
	
	@Test
	public void workWithIframes() 
	{
		driver.get("https://paytm.com/");
		driver.manage().window().maximize();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		myUtilis.returnWebElement("className","_3ac-").click();
		WebElement myFrame = myUtilis.returnWebElement("xpath","//iframe[contains(@src,'login')]");
		driver.switchTo().frame(myFrame);
		myUtilis.returnWebElement("name","username").sendKeys("nag@abc.com");
		myUtilis.returnWebElement("name","password").sendKeys("abcd");
		myUtilis.returnWebElement("cssSelector",".login-btn").click();
		
	}
	@Test
	public void workWithAlerts()
	{
		driver.get("http://www.apsrtconline.in");
		myUtilis.returnWebElement("name","fromPlaceName").sendKeys("Hyderabad");
		//myUtilis.returnWebElement("name","toPlaceName").sendKeys("Vijayawada");
		myUtilis.returnWebElement("name","searchBtn").sendKeys(Keys.ENTER);
		Alert myAlert = driver.switchTo().alert();
		System.out.println(myAlert.getText());
		myAlert.accept();
		myUtilis.returnWebElement("name","toPlaceName").sendKeys("Vijayawada");
		myUtilis.returnWebElement("name","searchBtn").sendKeys(Keys.ENTER);
		
	}
	@Test
	public void windowHandlers()
	{
		String title = driver.getTitle();
		System.out.println(title);
		Set<String> myWindows = driver.getWindowHandles();
		List<String> windows = new ArrayList<String>(myWindows);
		driver.switchTo().window(windows.get(1));
		driver.get("https://icicibank.com");
		System.out.println(driver.getTitle());
		//driver.close();
		driver.quit();
		/*for(int i=0;i<windows.size();i++)
		{
			System.out.println(windows.get(i));
		}*/
	}
	@Test
	public void workWithLinkText()
	{
		//driver.findElement(By.linkText("REGISTER")).click();
		myUtilis.returnWebElement("linkText","REGISTER").click();
	}
	@Test
	public void workWithPartialLinkText()
	{
		//driver.findElement(By.linkText("REGISTER")).click();
		myUtilis.returnWebElement("partialLinkText","SIGN").click();
		loginApplication_WithReturnWebElement();
		myUtilis.returnWebElement("partialLinkText","SIGN").click();
	}
	@Test
	public void workWithStaticXpath()
	{
		myUtilis.returnWebElement("xpath","/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/a").click();
	}
	
	@Test
	public void workWithRelativeXpath()
	{
		myUtilis.returnWebElement("xpath","//a[contains(@href,'mercuryregister')]").click();
	}
	@Test
	public void gmailLogin()
	{
		driver.get("https://gmail.com");
		myUtilis.returnWebElement("id","identifierId").sendKeys("nagseleniumm2017");
		WebElement nextButton = myUtilis.returnWebElement("cssSelector",".RveJvd");
		nextButton.click();
		myUtilis.returnWebElement("name","password").sendKeys("kothapalli@123456");
		myUtilis.returnWebElement("cssSelector",".RveJvd").click();
	}
	
	@Test
	public void loginApplication()
	{
		WebElement myUserName = driver.findElement(By.name("userName"));
		if(myUserName.isEnabled())
		{
			myUserName.sendKeys("nag022");
		}
		else
		{
			System.out.println("UserName field is disabled");
		}
		
		WebElement myPassWord = driver.findElement(By.name("password"));
		
		if(myPassWord.isEnabled())
		{
			myPassWord.sendKeys("mar1234");
		}
		else
		{
			System.out.println("UserName field is disabled");
		}
		driver.findElement(By.name("login")).click();
		
	}
	
	List<WebElement> myElements;
	WebElement myElement;
	@Test
	public void bookFlight()
	{
		loginApplication();
		/*returnWebElementFromCollection("tripType","oneway").click();
		returnWebElementFromCollection("tripType","roundtrip").click();
		returnWebElementFromCollection("servClass","First").click();
		returnWebElementFromCollection("servClass","Business").click();*/
		myUtilis.returnWebElementFromCollection("name","tripType","value","oneway").click();
		myUtilis.returnWebElementFromCollection("name","servClass","value","Business").click();
		myUtilis.selectAnElementFromDropDown("name","passCount",2);
		myUtilis.selectAnElementFromDropDown("name","fromPort","London");
		myUtilis.selectAnElementFromDropDown("name","toPort","Paris");
		/*WebElement passengers = driver.findElement(By.name("passCount"));
		Select mySelect = new Select(passengers);
		mySelect.selectByIndex(2);
		mySelect.selectByVisibleText("4");*/
		
		
		// new Select(driver.findElement(By.name("passCount"))).selectByIndex(2);
		
		//returnWebElementFromCollection("className","mouseOut","innerText","Cruises").click();
		/* myElements = driver.findElements(By.name("tripType"));
		for(int i=0;i<myElements.size();i++)
		{
			if(myElements.get(i).getAttribute("value").equals("oneway"))
			{
				myElements.get(i).click();
				break;
			}
		}*/
		
		 /*myElements = driver.findElements(By.name("servClass"));
		for(int i=0;i<myElements.size();i++)
		{
			if(myElements.get(i).getAttribute("value").equals("First"))
			{
				myElements.get(i).click();
				break;
			}
		}*/
		//myElements.get(1).click();
	}
	
}
