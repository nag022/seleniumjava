package seleniumPractice;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ApplicationUtilities 
{
	//app utils class
	WebElement myElement;
	List<WebElement> myElements;
	WebDriver driver;
	public ApplicationUtilities(WebDriver driver)
	{
		this.driver = driver;
	}
	public void selectAnElementFromDropDown(String propType,String propValue,int index)
	{
		 new Select(returnWebElement(propType,propValue)).selectByIndex(index);
	}
	public void selectAnElementFromDropDown(String propType,String propValue,String text)
	{
		 new Select(returnWebElement(propType,propValue)).selectByVisibleText(text);
	}
	public WebElement returnWebElement(String propType,String propValue)
	{
		if(propType.equalsIgnoreCase("name"))
			myElement = driver.findElement(By.name(propValue));
		else if(propType.equalsIgnoreCase("id"))
			myElement = driver.findElement(By.id(propValue));
		else if(propType.equalsIgnoreCase("className"))
			myElement = driver.findElement(By.className(propValue));
		else if(propType.equalsIgnoreCase("cssSelector"))
			myElement = driver.findElement(By.cssSelector(propValue));
		else if(propType.equalsIgnoreCase("linkText"))
			myElement = driver.findElement(By.linkText(propValue));
		else if(propType.equalsIgnoreCase("partialLinkText"))
			myElement = driver.findElement(By.partialLinkText(propValue));
		else if(propType.equalsIgnoreCase("tagName"))
			myElement = driver.findElement(By.tagName(propValue));
		else if(propType.equalsIgnoreCase("xpath"))
			myElement = driver.findElement(By.xpath(propValue));
		
		if(myElement.isEnabled())
			return myElement;
		else
			return null;
	}
	public List<WebElement> returnWebElements(String propType,String propValue)
	{
		if(propType.equalsIgnoreCase("name"))
			myElements = driver.findElements(By.name(propValue));
		else if(propType.equalsIgnoreCase("id"))
			myElements = driver.findElements(By.id(propValue));
		else if(propType.equalsIgnoreCase("className"))
			myElements = driver.findElements(By.className(propValue));
		else if(propType.equalsIgnoreCase("cssSelector"))
			myElements = driver.findElements(By.cssSelector(propValue));
		else if(propType.equalsIgnoreCase("linkText"))
			myElements = driver.findElements(By.linkText(propValue));
		else if(propType.equalsIgnoreCase("partialLinkText"))
			myElements = driver.findElements(By.partialLinkText(propValue));
		else if(propType.equalsIgnoreCase("tagName"))
			myElements = driver.findElements(By.tagName(propValue));
		else if(propType.equalsIgnoreCase("xpath"))
			myElements = driver.findElements(By.xpath(propValue));
		return myElements;
	}
	public WebElement returnWebElementFromCollection(String propType,String propValue,String att,String attValue)
	{
		myElements = returnWebElements(propType,propValue);
		
			for(int i=0;i<myElements.size();i++)
			{
				System.out.println(myElements.get(i).getAttribute(att));
				if(myElements.get(i).getAttribute(att).trim().equals(attValue.trim()))
				{
					myElement = myElements.get(i);
					break;
				}
			}
			return myElement;
	}
	public WebElement returnWebElementFromCollection(String propValue,String attValue)
	{
		 myElements = driver.findElements(By.name(propValue));
			for(int i=0;i<myElements.size();i++)
			{
				if(myElements.get(i).getAttribute("value").equals(attValue))
				{
					myElement = myElements.get(i);
					break;
				}
			}
			return myElement;
	}

}
