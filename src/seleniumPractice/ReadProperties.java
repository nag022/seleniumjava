package seleniumPractice;

import java.io.FileInputStream;
import java.util.Properties;

public class ReadProperties 
{
	FileInputStream myFile;
	Properties myProp;
	String filePath;
	/*public ReadProperties()
	{
		filePath = "TestData.properties";
	}*/
	public ReadProperties(String filePath)
	{
		this.filePath = filePath;
	}
	public String readProperties(String key)
	{
		try {
			myFile = new FileInputStream(filePath);
			myProp = new Properties();
			myProp.load(myFile);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Exception :"+ e);
		}
		
		return myProp.getProperty(key);
		/*System.out.println(myProp.getProperty("URL"));
		System.out.println(myProp.getProperty("UserName"));
		System.out.println(myProp.getProperty("PassWord"));*/
	}
}
