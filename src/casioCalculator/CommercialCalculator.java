package casioCalculator;

public class CommercialCalculator implements ArithematicOperations
{
	int sum;
	@Override
	public int addition(int a, int b) {
		System.out.println("Addition from CommercialCalculator ");
		sum = a + b;
		return sum;
	}

	@Override
	public int addition(int[] numbers) {
		System.out.println("Addition from CommercialCalculator ");
		for(int i=0;i<numbers.length;i++)
		{
			sum = sum + numbers[i];
		}
		return (sum);
	}

	@Override
	public int subtraction(int a, int b) {
		sum = a - b;
		return sum;
	}

	@Override
	public int multiplication(int a, int b) {
		sum = a * b;
		return sum;
	}

	@Override
	public int division(int a, int b) {
		sum = a / b;
		return sum;
	}
	
}
